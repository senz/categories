import { Component } from '@angular/core';
import { CategoryService } from './category.service';
import { map } from 'rxjs/operators';
import { interval } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(public categoryService : CategoryService){
    
  }
  title = 'categoties';

  filter(event : Event){  
    var word = (<HTMLInputElement>event.target).value
    this.categoryService.getCategories().pipe(map(data=>{
      return data.filter((res : any) => res.includes(word))
    })).subscribe(rs=>{
      this.categoryService.categories$.next(rs)
    })
  }

  ngOnInit() {    
        //this.getProductsUsingSubscribeMethod();    
    this.categoryService.getCategories().subscribe() 
  }  
}
