import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  categories$ : BehaviorSubject<any> = new BehaviorSubject<any>('')
  constructor(private http: HttpClient) { }

  getCategories() : Observable<any> {
    return this.http.get('api/categories').pipe(tap(rs=>{
      this.categories$.next(rs)
    }))
  }
}
